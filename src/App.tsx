import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter } from 'react-router-dom';

import { Customer } from './model/Customer';
import LocationComponent from './components/route/LocationComponent';
import { Bill } from './model/Bill';
import { User } from './model/User';
import { CustomersPagination } from './model/CustomersPagination';
import { FilterType } from './model/FilterType';
import HeaderComponent from './components/header/HeaderComponent';
import SwitchComponent from './components/route/SwitchComponent';
import { City } from './model/City';
import { State } from './model/State';
import { Seller } from './model/Seller';
import { CreditCard } from './model/CreditCard';
import * as _ from 'lodash';

interface AppState {
  originalCustomers: Customer[];
  customers : Customer[];
  bills : Bill[];
  newCustomerModalEnabled : false;
  customerBillsModalEnabled : false;
  selectedCustomerId? : number;
  user?: User;
  customersPagination?: CustomersPagination;
  filterType?: FilterType;
  previousFilterType?: FilterType;
  filterAscended: boolean;
  cities?: City[];
  states?: State[];
  sellers?: Seller[];
  creditCards?: CreditCard[];
  shouldFetchCustomers?: boolean;
}

export const AppContext = React.createContext({});

const initialState : AppState = {
  newCustomerModalEnabled: false,
  customerBillsModalEnabled: false,
  customers: [],
  originalCustomers: [],
  bills: [],
  customersPagination: {
    itemsPerPage: 25,
    currentPage: 0,
    totalPages: 0,
    customersPage: []
  },
  filterAscended: false
};

const reducer = (state : any, action : any) => {
  switch (action.type) {
    case "SHOW_NEW_CUSTOMER_MODAL":
      return {
        ...state,
        newCustomerModalEnabled: state.user?.Token ? true : false ,
      };
    case "HIDE_NEW_CUSTOMER_MODAL":
      return {
        ...state,
        newCustomerModalEnabled: false,
      };

    case "SHOW_CUSTOMER_BILLS_MODAL":
      return {
        ...state,
        customerBillsModalEnabled: state.user?.Token ? true : false,
        selectedCustomerId: action.payload
      };
    case "HIDE_CUSTOMER_BILLS_MODAL":
      return {
        ...state,
        customerBillsModalEnabled: false,
      };
    case "REGISTER_USER_SUCCESS":
        return {
          ...state,
          user: action.payload,
        };
    case "LOGIN_USER_SUCCESS":
      localStorage.setItem("username", JSON.stringify(action.payload.username));
      localStorage.setItem("token", JSON.stringify(action.payload.token));
      return {
        ...state,
        user: {...state.user!, Username : action.payload.username, Token : action.payload.token}
      };

    case "FETCH_USER_SUCCESS":
      localStorage.setItem("id", JSON.stringify(action.payload.id));
      return {
        ...state,
        user: {...state.user!, Id: action.payload.id, Name: action.payload.name, Username: action.payload.username, Img: action.payload.img}
      };

      
        
    case "EDIT_CUSTOMER_SAVE":
        const updatedCustomer = action.payload;
        const changedCustomers = (state as AppState).customers.map((customer) =>
            customer.Id === updatedCustomer.Id
              ? updatedCustomer
              : customer
          );
          console.log(changedCustomers);
        return {
          ...state,
          customers : changedCustomers,
        };
        case "SHOULD_FETCH_CUSTOMERS":
          return {
            ...state,
            shouldFetchCustomers: true
          };
        
    case "FETCH_CUSTOMERS_SUCCESS":
      return {
        ...state,
        customers: action.payload,
        originalCustomers: action.payload,
        shouldFetchCustomers: false
      };
    case "UPDATE_CUSTOMERS":
      return {
        ...state,
        customers: action.payload,
      };

    case "LOGOUT":
      localStorage.clear();

      return {
        ...state,
        user: {},
      };
      
    case "SHOW_CUSTOMER_BILL_DETAILS":
      return {
        ...state,
        selectedBillId: action.payload
      };
    case "FETCH_BILLS_SUCCESS":
      return {
        ...state,
        bills: action.payload
      };
    case "SETUP_CUSTOMERS_PAGINATION":
      return {
        ...state,
        customersPagination: {...state.customersPagination, customersPagination: action.payload}
      };

      case "SET_CURRENT_PAGE":
        return {
          ...state,
          customersPagination: {...state.customersPagination, currentPage: action.payload}
        };
        case "SET_ITEMS_PER_PAGE":
          return {
            ...state,
            customersPagination: {...state.customersPagination, itemsPerPage: action.payload}
          };
        case "SET_FILTER":
          return {
            ...state,
            previousFilterType: state.filterType,
            filterType: action.payload,
            filterAscended: state.filterType === action.payload ? !state.filterAscended : true
          };
          case "FETCH_CITIES_SUCCESS":
            return {
              ...state,
              cities: action.payload,
            };

            case "FETCH_STATES_SUCCESS":
              return {
                ...state,
                states: action.payload,
              };
              case "FETCH_SELLERS_SUCCESS":
                return {
                  ...state,
                  sellers: action.payload,
                };
          
                case "SEARCH_CUSTOMERS":
                return {
                  ...state,
                  search: action.payload,
                };

                case "FETCH_CREDIT_CARDS_SUCCESS":
                return {
                  ...state,
                  creditCards: action.payload.sort((a: CreditCard, b: CreditCard) => a.Id < b.Id).slice(0, 10),
                };

                
        

        

    default:
      return state;
  }
};

function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  React.useEffect(() => {
    const username = localStorage.getItem("username") ? JSON.parse(localStorage.getItem("username")!) : null
    const token = localStorage.getItem("token") ? JSON.parse(localStorage.getItem("token")!) : null
    const id = localStorage.getItem("id") ? JSON.parse(localStorage.getItem("id")!) : null

    if(username && token){
      dispatch({
        type: "LOGIN_USER_SUCCESS",
        payload: {
          username,
          token,
          id
        }
      })
    }
  }, [])

  React.useEffect(() => {
    dispatch({
      type: "FETCH_CUSTOMERS_REQUEST"
    });

    
    // fetch("http://www.fulek.com/nks/api/aw/customers", {
    fetch("http://www.fulek.com/nks/api/aw/last200customers", {
      // headers: {
      //   Authorization: `Token ${authState.token}`
      // }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then((resJson) => {
        console.log(resJson);
        dispatch({
          type: "FETCH_CUSTOMERS_SUCCESS",
          payload: resJson
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: "FETCH_CUSTOMERS_FAILURE"
        });
      });
  }, [state.shouldFetchCustomers]);

  React.useEffect(() => {
    dispatch({
      type: "FETCH_CITIES_REQUEST"
    });

    fetch("http://www.fulek.com/nks/api/aw/cities", {
      // headers: {
      //   Authorization: `Token ${authState.token}`
      // }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then((resJson) => {
        console.log(resJson);
        dispatch({
          type: "FETCH_CITIES_SUCCESS",
          payload: resJson
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: "FETCH_CITIES_FAILURE"
        });
      });
  }, []);

  React.useEffect(() => {
    dispatch({
      type: "FETCH_CREDIT_CARDS_REQUEST"
    });

    fetch("http://www.fulek.com/nks/api/aw/creditCards", {
      // headers: {
      //   Authorization: `Token ${authState.token}`
      // }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then((resJson) => {
        console.log(resJson);
        dispatch({
          type: "FETCH_CREDIT_CARDS_SUCCESS",
          payload: resJson
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: "FETCH_CREDIT_CARDS_FAILURE"
        });
      });
  }, []);

  

  React.useEffect(() => {
    dispatch({
      type: "FETCH_USER_REQUEST"
    });
console.log("fetch user started")
    fetch("http://www.fulek.com/nks/api/aw/getUser", {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username: state?.user?.Username,
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then((resJson) => {
        console.log(resJson);
        dispatch({
          type: "FETCH_USER_SUCCESS",
          payload: resJson
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: "FETCH_USER_FAILURE"
        });
      });
  }, [state.user?.Token]);

//   curl --location --request POST 'http://www.fulek.com/nks/api/aw/getUser' \
// --header 'Content-Type: application/json' \
// --data-raw '{
// 	"username":"test@user.com"
// }'

  React.useEffect(() => {
    dispatch({
      type: "FETCH_STATES_REQUEST"
    });

    fetch("http://www.fulek.com/nks/api/aw/states", {
      // headers: {
      //   Authorization: `Token ${authState.token}`
      // }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then((resJson) => {
        console.log(resJson);
        dispatch({
          type: "FETCH_STATES_SUCCESS",
          payload: resJson
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: "FETCH_CITIES_FAILURE"
        });
      });
  }, []);

  React.useEffect(() => {
    dispatch({
      type: "FETCH_SELLERS_REQUEST"
    });

    fetch("http://www.fulek.com/nks/api/aw/sellers", {
      // headers: {
      //   Authorization: `Token ${authState.token}`
      // }
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then((resJson) => {
        console.log(resJson);
        dispatch({
          type: "FETCH_SELLERS_SUCCESS",
          payload: resJson
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: "FETCH_SELLERS_FAILURE"
        });
      });
  }, []);

  

  const setupCustomersPagination = () => {
    const itemsPerPage = state.customersPagination.itemsPerPage;
    const customersLength = state.customers.length;
    const totalPages = customersLength / itemsPerPage;
    const totalPagesCeil = Math.ceil(totalPages);
    
    console.log(`customersLength: ${customersLength} itemsPerPage: ${itemsPerPage} totalPagesCeil: ${totalPagesCeil}`);
    const customersPagination = state.customersPagination;

    for(let i = 0; i < totalPagesCeil; i++) {
      customersPagination.customersPage[i] = {};
      const sliceStart = (i * itemsPerPage);
      const sliceEnd = ((i + 1) * itemsPerPage);
      const customers = state.customers.slice(sliceStart, sliceEnd);
      customersPagination.customersPage[i].customers = customers;
    }

    if(state.customers.length <= 0) {
      customersPagination.customersPage = [];
      customersPagination.customersPage[0] = {
        customers: []
      }
    }
    customersPagination.totalPages = totalPagesCeil;
    dispatch({type: "SETUP_CUSTOMERS_PAGINATION", payload: customersPagination});
  };
  React.useEffect(() => {
    console.log("tusam")
    setupCustomersPagination();
    
  }, [state.customers]);

  React.useEffect(() => {
    setupCustomersPagination();
    
  }, [state.customersPagination.itemsPerPage]);

  React.useEffect(() => {

    let filteredCustomers = state.originalCustomers.filter((customer: Customer) => (customer.Name.toLowerCase().indexOf(state.search.toLowerCase()) > -1 || customer.Surname.toLowerCase().indexOf(state.search.toLowerCase()) > -1 || customer.Email.toLowerCase().indexOf(state.search.toLowerCase()) > -1) );
    // if(state.search === "") {
    //   filteredCustomers = state.originalCustomers;
    // }
   
    console.log(`filtered: ${JSON.stringify(filteredCustomers)}` )
    dispatch({
      type: "UPDATE_CUSTOMERS",
      payload: filteredCustomers
    });
    setupCustomersPagination();
  }, [state.search]);
  

  React.useEffect(() => {
    const customers = state.customers;
    const customersPagination = state.customersPagination;
    let sortedCustomers = [];
    
    switch(state.filterType) {
      case FilterType.Id:
        sortedCustomers = customers.sort((a : Customer, b : Customer) => state.filterAscended ? a.Id > b.Id : a.Id < b.Id);
        break;

      case FilterType.Name:
        sortedCustomers = customers.sort((a : Customer, b : Customer) => state.filterAscended ? a.Name > b.Name : a.Name < b.Name);
        break;

      case FilterType.Surname:
        sortedCustomers = customers.sort((a : Customer, b : Customer) => state.filterAscended ? a.Surname > b.Surname : a.Surname < b.Surname);
        break;

      case FilterType.Email:
        sortedCustomers = customers.sort((a : Customer, b : Customer) => state.filterAscended ? a.Email > b.Email : a.Email < b.Email);
      break;
      case FilterType.Telephone:
        sortedCustomers = customers.sort((a : Customer, b : Customer) => state.filterAscended ? a.Telephone > b.Telephone : a.Telephone < b.Telephone);
        break;

      case FilterType.City:
        sortedCustomers = customers.sort((a : Customer, b : Customer) => state.filterAscended ? a.City > b.City : a.City < b.City);
        break;
      }

      // useEffect ima  [state.customers] ko dependeny i svejedno se ne okida metoda, zato rucno setupCustomersPagination
      dispatch({
        type: "FETCH_CUSTOMERS_SUCCESS",
        payload: sortedCustomers
      });
      setupCustomersPagination();
  }, [state.filterType, state.filterAscended]);

  return (
    <AppContext.Provider
    value={{
      state,
      dispatch
    }}
    >
      <div className="App">
        <BrowserRouter  forceRefresh={false}>
          <div className="container">
            <HeaderComponent />
            <LocationComponent />
            <SwitchComponent />
          </div>
        </BrowserRouter>


      </div>
    </AppContext.Provider>
  );
}

export default App;
