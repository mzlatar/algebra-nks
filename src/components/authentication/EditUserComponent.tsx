import React from "react";
import Button from "react-bootstrap/Button";
import { Table, ButtonToolbar } from "react-bootstrap";
import {withRouter} from 'react-router';
import { useHistory, useLocation } from "react-router-dom";
import { User } from "../../model/User";
import { AppContext } from "../../App";

interface EditUserState {
  user?: User,
}

export const EditUserComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const { push } = useHistory();

    const initialState : EditUserState = {
        user : appState.user
      };
    const [state, setState] = React.useState(initialState);
      

    React.useEffect(() => {
      
      setState({...state, user: appState.user});
    }, [appState.user?.Name]);

    const handleOnClick = () => {
      
      dispatch({
        type: "EDIT_USER_STARTED",
      });

      console.log(`edit user ${JSON.stringify({
        id: `${state?.user?.Id}`,
        name: state?.user?.Name,
        username: state?.user?.Username,
        password: state?.user?.Password,
        img: "123"
      })}`)
      
      fetch(`http://www.fulek.com/nks/api/aw/editUser`, {
        method: "post",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id: `${state?.user?.Id}`,
          name: state?.user?.Name,
          username: state?.user?.Username,
          password: state?.user?.Password,
          img: "123"
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          dispatch({
            type: "EDIT_USER_SUCCESS",
            payload: resJson
          });
        push("/customers");


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "EDIT_USER_FAILURE"
          });
        });
      
    };

    return (
        
      <div className="container">
      <form>

        <label htmlFor="name">Name</label>
            <input
            id="name"
            name="name"
            type="text"
            value={state.user?.Name || ""}
            onChange={e => setState({...state, user: {...state.user!, Name : e.target.value}})}
            className="text-input"
            />
        <br/>
        <label htmlFor="username">Username</label>
            <input
            id="username"
            name="username"
            type="text"
            value={state.user?.Username || ""}
            onChange={e => setState({...state, user: {...state.user!, Username : e.target.value}})}
            className="text-input"
            />

        <br/>
        <label htmlFor="password">Password</label>
            <input
            id="password"
            name="password"
            type="text"
            value={state.user?.Password || ""}
            onChange={e => setState({...state, user: {...state.user!, Password : e.target.value}})}
            className="text-input"
            />
        <br/>
        <label htmlFor="img">Avatar</label>
            <input
            id="img"
            name="img"
            type="text"
            value={state.user?.Img || ""}
            onChange={e => setState({...state, user: {...state.user!, Img : e.target.value}})}
            className="text-input"
            />
          
        
          <ButtonToolbar >

          <Button variant="primary" onClick={handleOnClick}>Save</Button>
          </ButtonToolbar>




      {/* <div className="form-error">
            <p>
              {state.customerHasError && "Error Creating customer!"}
            </p>
      </div>
      <div className="form-action clearfix">
          <button
            type="button"
            id="overlay-confirm-button"
            className="button button-primary"
            onClick={onSubmit}
            disabled={state.isButtonDisabled}
          >
            {state.isSubmitting ? "Submitting..." : "Submit"}
          </button>
          <button
            type="button"
            id="overlay-cancel-button"
            className="button button-default small close-overlay pull-right"
            onClick={onClose}
          >
                Cancel
          </button>
      </div>  */}
  </form>         
</div> 
        

        
  );
};

export default EditUserComponent;
