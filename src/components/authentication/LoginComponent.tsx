import React from "react";
import Button from "react-bootstrap/Button";
import { ButtonToolbar } from "react-bootstrap";
import { AppContext } from "../../App";
import { useHistory } from "react-router-dom";
             

interface LoginUserState {
  username? : string;
  password? : string;
}

export const LoginComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const { push } = useHistory();

    const initialState : LoginUserState = {
        
      };
    const [state, setState] = React.useState(initialState);
      
    const handleOnClick = () => {
      
      dispatch({
        type: "LOGIN_REQUEST_STARTED",
      });


      
      fetch(`http://www.fulek.com/nks/api/aw/login`, {
        method: "post",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          username: state?.username,
          password: state?.password,
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          dispatch({
            type: "LOGIN_USER_SUCCESS",
            payload: resJson
          });
        push("/customers");


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "REGISTER_USER_FAILURE"
          });
        });
      
    };

    return (
        
      <div className="container">
      <form>

   
        <label htmlFor="username">Username</label>
            <input
            id="username"
            name="username"
            type="text"
            value={state.username || ""}
            onChange={e => setState({...state, username: e.target.value})}
            className="text-input"
            />

        <br/>
        <label htmlFor="password">Password</label>
            <input
            id="password"
            name="password"
            type="text"
            value={state.password || ""}
            onChange={e => setState({...state, password : e.target.value})}
            className="text-input"
            />
        <br/>
        
          <ButtonToolbar >

          <Button variant="primary" onClick={handleOnClick}>Login</Button>
          </ButtonToolbar>




      {/* <div className="form-error">
            <p>
              {state.customerHasError && "Error Creating customer!"}
            </p>
      </div>
      <div className="form-action clearfix">
          <button
            type="button"
            id="overlay-confirm-button"
            className="button button-primary"
            onClick={onSubmit}
            disabled={state.isButtonDisabled}
          >
            {state.isSubmitting ? "Submitting..." : "Submit"}
          </button>
          <button
            type="button"
            id="overlay-cancel-button"
            className="button button-default small close-overlay pull-right"
            onClick={onClose}
          >
                Cancel
          </button>
      </div>  */}
  </form>         
</div> 
        

        
  );
};

export default LoginComponent;
