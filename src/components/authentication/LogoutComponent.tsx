import React from "react";
import Button from "react-bootstrap/Button";
import { ButtonToolbar } from "react-bootstrap";
import { AppContext } from "../../App";
import { useHistory } from "react-router-dom";
             

export const LogoutComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const { push } = useHistory();
    dispatch({
      type: "LOGOUT",
    });
    push("/customers");
 
      

    return (
        <></>
        

        
  );
};

export default LogoutComponent;
