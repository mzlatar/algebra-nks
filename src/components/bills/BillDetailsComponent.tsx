import React from "react";
import { Table } from "react-bootstrap";
import { AppContext } from "../../App";
import { useHistory } from "react-router-dom";
import BillItemRowComponent from "./BillItemRowComponent";
import NewBillItemComponent from "./NewBillItemComponent";
             

export const BillDetailsComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const history = useHistory();
    const initialState = {
        billItems : []
      };
    const [state, setState] = React.useState(initialState);
    
    React.useEffect(() => {
        dispatch({
          type: "FETCH_BILL_ITEMS_REQUEST"
        });
        if(appState.selectedBillId) {
        
        fetch(`http://www.fulek.com/nks/api/aw/billitems/${appState.selectedBillId}`, {
          // headers: {
          //   Authorization: `Token ${authState.token}`
          // }
        })
          .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw res;
            }
          })
          .then((resJson) => {
            console.log(resJson);
          
            setState({...state, billItems: resJson});
          })
          .catch(error => {
            console.log(error);
            dispatch({
              type: "FETCH_BILLS_FAILURE"
            });
          });
        }
      }, [appState.selectedBillId]);

    // router.push('/foo')
    
    return (
        
        <div>
           <NewBillItemComponent billId={appState.selectedBillId}/>
            <div className="mt-5 d-flex justify-content-center">
             
                <Table className="mt-4" striped bordered hover size="sm"> 
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Color</th>
                        <th>Quantity</th>
                        <th>Price per piece</th>
                        <th>Total price</th>
                    </tr>
                </thead>
                <tbody>
                    
                    {
                        state.billItems && state.billItems.length > 0 &&
                        state.billItems.map((billItem : any) => (
                                <BillItemRowComponent billItem={billItem} key={billItem.Id.toString()}/>
                                
                            ))
                    }
                    
                </tbody>
                </Table>
           
            
            </div>
        </div>
        

        
  );
};

export default BillDetailsComponent;
