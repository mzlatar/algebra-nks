import React from "react";
import { useHistory } from 'react-router'
import { AppContext } from "../../App";
import { BillItem } from "../../model/BillItem";
import { BillItemProps } from "../../props/BillItemProps";

interface BillItemRowComponentState {
  isClicked : boolean;
  billItem : BillItem;
}
export const BillItemRowComponent : React.FunctionComponent<BillItemProps> = ({billItem}) => {
  const { push } = useHistory();
  const initialState : BillItemRowComponentState = {
    isClicked : false,
    billItem : billItem,
  };
  const [state, setState] = React.useState(initialState);

  const { state : appState, dispatch } = React.useContext(AppContext) as any;

  
  // const handleOnClick = () => {
  //   push(`/customer/${customer.Id}/bills`);
    
  //   dispatch({
  //     type: "SHOW_CUSTOMER_BILLS_MODAL",
  //     payload: customer.Id
  //   });
  // };

  // const handleOnEditClick = (event : any) => {
  //   event.stopPropagation();
  //   const isClicked = !state.isClicked;
    
  //   const editingCustomer = {...state.customer};
  //   setState({...state, isClicked, editingCustomer : editingCustomer});
  // };

  const handleOnDeleteClick = (event : any) => {
    fetch(`http://www.fulek.com/nks/api/aw/deleteItem`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${appState.user.Token}`
        },
        body: JSON.stringify({
          id: billItem.Id
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          // dispatch({
          //   type: "REGISTER_USER_SUCCESS",
          //   payload: resJson
          // });
        push(`/bill/${state.billItem.BillId}`);


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "REGISTER_USER_FAILURE"
          });
        });
    push("/customers");

  };

  // const handleOnCancelClick = (event : any) => {
  //   event.stopPropagation();
  //   const isClicked = false;
  //   setState({...state, isClicked, editingCustomer : undefined});
  // };
  
  // const handleInputChange = (event : any) => {
  //   const updatedCustomer = state.editingCustomer!;
  //   (updatedCustomer as any)[event.target.name] = event.target.value;

  //   setState({
  //     ...state,
  //     editingCustomer: updatedCustomer
  //   });
  // };
 

  return (
    <tr>
    <React.Fragment >
      <td>{state.billItem.Id}</td>
      <td>{state.billItem.Product.Name}</td>
      <td>{state.billItem.Product.ProductNumber}</td>
      <td>{state.billItem.Product.Color}</td>
      <td>{state.billItem.Quantity}</td>
      <td>{state.billItem.PricePerPiece}</td>
      <td>{state.billItem.TotalPrice}</td>
      {/* <td><button onClick={handleOnEditClick}>edit</button></td> */}
        <td><button onClick={handleOnDeleteClick}>delete</button></td>
    </React.Fragment>
  </tr>
  ) 
};

export default BillItemRowComponent;
