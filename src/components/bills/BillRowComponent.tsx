import React from "react";
import { useHistory } from 'react-router'
import { AppContext } from "../../App";
import { Bill } from "../../model/Bill";
import { BillProps } from "../../props/BillProps";
import './BillRow.css'
interface BillRowComponentState {
  isClicked : boolean;
  bill : Bill;
  editingBill? : Bill;
}
export const BillRowComponent : React.FunctionComponent<BillProps> = ({bill}) => {
  const { push } = useHistory();
  const initialState : BillRowComponentState = {
    isClicked : false,
    bill : bill,
    editingBill : undefined
  };
  const [state, setState] = React.useState(initialState);
  const history = useHistory();

  const { state : appState, dispatch } = React.useContext(AppContext) as any;

  
  const handleOnClick = () => {
    // push(`/customers/id/${state.customer.Id}`)
    history.push(`/bill/${bill.Id}`);
  };

  const handleOnEditClick = (event : any) => {
    event.stopPropagation();
    const isClicked = !state.isClicked;
    
    const editingBill = {...state.bill};
    setState({...state, isClicked, editingBill : editingBill});
  };

  const handleOnSaveClick = (event : any) => {
    event.stopPropagation();
    const isClicked = false;
    dispatch({
      type: "EDIT_BILL_SAVE",
      payload: state.editingBill!
    });
    setState({...state, bill : state.editingBill!, editingBill : undefined, isClicked});
  };

  const handleOnCancelClick = (event : any) => {
    event.stopPropagation();
    const isClicked = false;
    setState({...state, isClicked, editingBill : undefined});
  };
  
  const handleInputChange = (event : any) => {
    const updatedBill = state.editingBill!;
    (updatedBill as any)[event.target.name] = event.target.value;

    setState({
      ...state,
      editingBill: updatedBill
    });
  };
 

  return (
  
    <tr onClick={handleOnClick}>
    <React.Fragment >
      <td>{state.bill?.Id}</td>
      <td>{state.bill?.Date}</td>
      <td>{state.bill?.BillNumber}</td>
      <td>{state.bill?.CreditCard?.Type}</td>
      <td>{state.bill?.CreditCard?.CardNumber}</td>
        <td><button>delete</button></td>
    </React.Fragment>
  </tr>
  ) 
};

export default BillRowComponent;
