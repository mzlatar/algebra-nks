import React from "react";
import { Customer } from "../../model/Customer";
import {Modal, Button, Row, Col, Form, Table, ButtonToolbar} from "react-bootstrap";
import { AppContext } from "../../App";
import { Bill } from "../../model/Bill";
import BillRowComponent from "./BillRowComponent";
import { useHistory } from "react-router-dom";

interface CustomerBillsState {
  bills: Bill[],
}



interface CustomerBillsModalProps {
  show : boolean;
  onHide : Function;
  animation? : boolean;
  customerId?: number;
}
export const CustomerBillsModalComponent : React.FunctionComponent<CustomerBillsModalProps> = (props) => {

    const initialState = {
      bills : []
    };

    const history = useHistory();

    console.log(`customerid: ${props.customerId}`)
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const customerId = props.customerId ? props.customerId : appState.selectedCustomerId;
    console.log("book from custome bills modal " + customerId)
    
    React.useEffect(() => {
      console.log(`fetch bills from customer ${customerId}`)
      dispatch({
        type: "FETCH_BILLS_REQUEST"
      });
      if(customerId) {
      
      fetch(`http://www.fulek.com/nks/api/aw/customerbills/${customerId}`, {
        // headers: {
        //   Authorization: `Token ${authState.token}`
        // }
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          setState({...state, bills: resJson});
        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "FETCH_BILLS_FAILURE"
          });
        });
      }
    }, [appState.selectedCustomerId]);

    const [state, setState] = React.useState(initialState);

    const onClose = () => {};
    
  return (
    <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Customer bills
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="container-fluid">
            <Button variant="primary" onClick={() => { 
                history.push("/bills/new");
            }}>New bill</Button>
            <div className="mt-5 d-flex justify-content-center">

             <Table responsive="lg" className="mt-4" striped bordered hover size="sm" style={{maxWidth:"50px"}}> 
             <thead>
                 <tr>
                     <th>Id</th>
                     <th>Date</th>
                     <th>Bill number</th>
                     <th>Card type</th>
                     <th>Card number</th>
                 </tr>
             </thead>
             <tbody>
                 
                 {
                     state.bills && state.bills.length > 0 &&
                        state.bills.map((bill : any) => (
                             <BillRowComponent bill={bill} key={bill.Id.toString()}/>
                             
                         ))
                 }
                 
             </tbody>
             </Table>
        
         
         </div>   
            </div> 
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => dispatch({
                    type: "HIDE_CUSTOMER_BILLS_MODAL",
                })}>Close</Button>
        </Modal.Footer>
      </Modal>
  );
};
export default CustomerBillsModalComponent;


  