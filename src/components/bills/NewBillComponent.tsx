


import React from "react";
import { Button, ButtonToolbar } from "react-bootstrap";
import { AppContext } from "../../App";
import { BillItem } from "../../model/BillItem";
import { Category } from "../../model/Category";
import { SubCategory } from "../../model/SubCategory";
import { Product } from "../../model/Product";
import { NewBillItemProps } from "../../props/NewBillItemProps";
import { useHistory } from "react-router-dom";
import { Customer } from "../../model/Customer";
import { Seller } from "../../model/Seller";
import DatePicker from "react-datepicker";
 
import "react-datepicker/dist/react-datepicker.css";
import { ScriptTarget } from "typescript";
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import { CreditCard } from "../../model/CreditCard";
registerLocale('hr', hr)
             
interface NewBillComponentState {
  date?: Date;
  billNumber?: string;
  customerId?: number;
  sellerId?: number;
  comment?: string;
  creditCardId?: number;
}

export const NewBillComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const { push } = useHistory();

    const initialState : NewBillComponentState = {
      date: new Date(),
      customerId: Number(appState.selectedCustomerId),
      sellerId: appState.sellers ? appState.sellers[0].Id : 0,
      creditCardId: appState.creditCards ? appState.creditCards[0].Id : 0
      };
    const [state, setState] = React.useState(initialState);
      

    
    

    const handleOnClick = () => {
      
      dispatch({
        type: "NEW_BILL_REQUEST_STARTED",
      });
   
      
      fetch(`http://www.fulek.com/nks/api/aw/addbill`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${appState.user.Token}`
        },
        body: JSON.stringify({
          "Date": state.date,
          "BillNumber": state.billNumber,
          "CustomerId": state.customerId,
          "SellerId": state.sellerId,
          "CreditCardId": state.creditCardId,
          "Comment": state.comment
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          // dispatch({
          //   type: "REGISTER_USER_SUCCESS",
          //   payload: resJson
          // });
        push("/customers");


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "REGISTER_USER_FAILURE"
          });
        });
      
    };

    return (
        


      // {
      //   "Date": "2019-01-01",
      //   "BillNumber": "BL12345",
      //   "CustomerId": 101,
      //   "SellerId": 276,
      //   "CreditCardId": 9458,
      //   "Comment": "No comment"
      // }
      
      



      <div className="container">
      <form>
     
        <label htmlFor="date">Date</label>

        <DatePicker dateFormat="dd.MM.yyyy" locale="hr" onChange={(date:Date) => setState({...state, date})} selected={state.date}  />
        <br/>

        <label htmlFor="bill-number">BillNumber</label>
        <input
            id="bill-number"
            name="bill-number"
            type="text"
            value={state.billNumber || ""}
            onChange={e => setState({...state, billNumber: e.target.value})}
            className="text-input"
            />
        <br/>

       <label htmlFor="customer-select">Customer</label>
            <select name="customer-select" id="customer-select" value={state.customerId} onChange={e => setState({...state, customerId: Number(e.target.value)})}>
              {appState.customers?.map((customer: Customer) => (
                <option value={customer.Id} key={`cusotmer-${customer.Id}`}>{`${customer.Name} ${customer.Surname}`}</option>
              ))}
              
            </select>
        <br/>

        <label htmlFor="seller-select">Seller</label>
            <select name="seller-select" id="seller-select" onChange={e => setState({...state, sellerId: Number(e.target.value)})}>
              {appState.sellers?.map((seller: Seller) => (
                <option value={seller.Id} key={`seller-${seller.Id}`}>{`${seller.Name} ${seller.Surname}`}</option>
              ))}
              
            </select>
        <br/>

        <label htmlFor="credit-card-select">Credit card</label>
            <select name="credit-card-select" id="credit-card-select" onChange={e => setState({...state, creditCardId: Number(e.target.value)})}>
              {appState.creditCards?.map((creditCard: CreditCard) => (
                <option value={creditCard.Id} key={`credit-card-${creditCard.Id}`}>{`${creditCard.Type.toString()}`}</option>
              ))}
              
            </select>
        <br/>

        {/* <label htmlFor="credit-card-select">Credit card</label>
            <select name="credit-card-select" id="credit-card-select" onChange={e => setState({...state, creditCardId: Number(e.target.value)})}>
              {state.creditCards?.map((creditCard: CreditCard) => (
                <option value={creditCard.Id} key={`creditCard-${creditCard.Id}`}>{`${creditCard.Name}`}</option>
              ))}
              
            </select>
        <br/> */}

        <label htmlFor="comment">Comment</label>
        <input
            id="comment"
            name="comment"
            type="text"
            value={state.comment || ""}
            onChange={e => setState({...state, comment: e.target.value})}
            className="text-input"
            />
        <br/>

       
        
          <ButtonToolbar >

          <Button variant="primary" onClick={handleOnClick}>Save</Button>
          </ButtonToolbar>




      {/* <div className="form-error">
            <p>
              {state.customerHasError && "Error Creating customer!"}
            </p>
      </div>
      <div className="form-action clearfix">
          <button
            type="button"
            id="overlay-confirm-button"
            className="button button-primary"
            onClick={onSubmit}
            disabled={state.isButtonDisabled}
          >
            {state.isSubmitting ? "Submitting..." : "Submit"}
          </button>
          <button
            type="button"
            id="overlay-cancel-button"
            className="button button-default small close-overlay pull-right"
            onClick={onClose}
          >
                Cancel
          </button>
      </div>  */}
  </form>         
</div> 
        

        
  );
};

export default NewBillComponent;


