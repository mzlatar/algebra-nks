


import React from "react";
import { Button, ButtonToolbar } from "react-bootstrap";
import { AppContext } from "../../App";
import { BillItem } from "../../model/BillItem";
import { Category } from "../../model/Category";
import { SubCategory } from "../../model/SubCategory";
import { Product } from "../../model/Product";
import { NewBillItemProps } from "../../props/NewBillItemProps";
import { useHistory } from "react-router-dom";
             
interface NewBillItemComponentState {
  billItem? : BillItem;
  selectedCategoryId?: number;
  selectedSubCategoryId?: number;
  selectedProductId?: number;
  selectedProduct?: Product;
  quantity?: number;
  categories?: Category[];
  subCategories?: SubCategory[];
  products?: Product[];
}

export const NewBillItemComponent : React.FunctionComponent<NewBillItemProps> = ({billId}) => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const history = useHistory();

    const initialState : NewBillItemComponentState = {
        quantity: 1
      };
    const [state, setState] = React.useState(initialState);
      

    React.useEffect(() => {
      fetch("http://www.fulek.com/nks/api/aw/categories", {
     
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
          setState({...state, categories: resJson, selectedCategoryId: resJson[0].Id})
        })
        .catch(error => {
          console.log(error);
        });

      
    }, []);

    React.useEffect(() => {
      fetch(`http://www.fulek.com/nks/api/aw/subcategories/${state.selectedCategoryId}`, {
        // headers: {
        //   Authorization: `Token ${authState.token}`
        // }
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
          setState({...state, subCategories: resJson, selectedSubCategoryId: resJson[0].Id})
        })
        .catch(error => {
          console.log(error);
        });

      
    }, [state.selectedCategoryId]);

    React.useEffect(() => {
      setState({...state, selectedProduct: state.products?.filter((product) => product.Id === state.selectedProductId)[0]})

      
    }, [state.selectedProductId]);


    React.useEffect(() => {
      fetch(`http://www.fulek.com/nks/api/aw/products/${state.selectedSubCategoryId}`, {
        // headers: {
        //   Authorization: `Token ${authState.token}`
        // }
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
          setState({...state, products: resJson, selectedProductId: resJson[0].Id})
        })
        .catch(error => {
          console.log(error);
        });

      
    }, [state.selectedSubCategoryId]);
    
    

    const handleOnClick = () => {
      
      dispatch({
        type: "NEW_BILL_ITEM_REQUEST_STARTED",
      });

      console.log(`token: ${appState.user.Token} billId: ${billId} productId: ${state.selectedProductId} quantity: ${state.quantity}`);
      
      fetch(`http://www.fulek.com/nks/api/aw/additem`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${appState.user.Token}`
        },
        body: JSON.stringify({
          BillId: billId,
          ProductId: state.selectedProductId,
          Quantity: state.quantity
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          history.push("/customers")
        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "REGISTER_USER_FAILURE"
          });
        });
      
    };

    return (
        
      <div className="container">
      <form>
      {/* add item: dropdowns -> category, subcategory, products, quantity, labele -> id, name color, product number */}
        <label htmlFor="category-select">Category</label>
            <select name="category-select" id="category-select" onChange={e => setState({...state, selectedCategoryId: Number(e.target.value)})}>
              {state.categories?.map((category) => (
                <option value={category.Id} key={`category-${category.Id}`}>{category.Name}</option>
              ))}
              
            </select>
        <br/>
        <label htmlFor="sub-category-select">SubCategory</label>
            <select name="sub-category-select" id="sub-category-select" onChange={e => setState({...state, selectedSubCategoryId: Number(e.target.value)})}>
              {state.subCategories?.map((subCategory) => (
                <option value={subCategory.Id} key={`sub-category-${subCategory.Id}`}>{subCategory.Name}</option>
              ))}
              
            </select>
        <br/>

        <label htmlFor="product-select">Product</label>
            <select name="product-select" id="product-select" onChange={e => setState({...state, selectedProductId: Number(e.target.value)})}>
              {state.products?.map((product) => (
                <option value={product.Id} key={`product-${product.Id}`}>{product.Name}</option>
              ))}
              
            </select>
        <br/>

        <label htmlFor="name">Quantity</label>
            <input
            id="quantity"
            name="quantity"
            type="text"
            value={state.quantity || ""}
            onChange={e => setState({...state, quantity: Number(e.target.value)})}
            className="text-input"
            />
        <br/>
          
        
          <ButtonToolbar >

          <Button variant="primary" onClick={handleOnClick}>Save</Button>
          </ButtonToolbar>




      {/* <div className="form-error">
            <p>
              {state.customerHasError && "Error Creating customer!"}
            </p>
      </div>
      <div className="form-action clearfix">
          <button
            type="button"
            id="overlay-confirm-button"
            className="button button-primary"
            onClick={onSubmit}
            disabled={state.isButtonDisabled}
          >
            {state.isSubmitting ? "Submitting..." : "Submit"}
          </button>
          <button
            type="button"
            id="overlay-cancel-button"
            className="button button-default small close-overlay pull-right"
            onClick={onClose}
          >
                Cancel
          </button>
      </div>  */}
  </form>         
</div> 
        

        
  );
};

export default NewBillItemComponent;


