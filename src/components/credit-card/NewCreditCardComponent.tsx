


import React from "react";
import { Button, ButtonToolbar } from "react-bootstrap";
import { AppContext } from "../../App";
import { BillItem } from "../../model/BillItem";
import { Category } from "../../model/Category";
import { SubCategory } from "../../model/SubCategory";
import { Product } from "../../model/Product";
import { NewBillItemProps } from "../../props/NewBillItemProps";
import { useHistory } from "react-router-dom";
import { Customer } from "../../model/Customer";
import { Seller } from "../../model/Seller";
import DatePicker from "react-datepicker";
 
import "react-datepicker/dist/react-datepicker.css";
import { ScriptTarget } from "typescript";
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import hr from 'date-fns/locale/hr';
import { CreditCard } from "../../model/CreditCard";
registerLocale('hr', hr)
             
interface NewCreditCardComponentState {
  type?: string;
  cardNumber?: string;
  expirationMonth?: number;
  expirationYear?: number;

}

export const NewCreditCardComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const { push } = useHistory();

    const initialState : NewCreditCardComponentState = {
      
      };
    const [state, setState] = React.useState(initialState);
      

    const handleOnClick = () => {
      
      dispatch({
        type: "NEW_CREDIT_CARD_REQUEST_STARTED",
      });
   
      
      fetch(`http://www.fulek.com/nks/api/aw/addcreditcard`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${appState.user.Token}`
        },
        body: JSON.stringify({
          "Type": state.type,
          "CardNumber": state.cardNumber,
          "ExpirationMonth": state.expirationMonth,
          "ExpirationYear": state.expirationYear,
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          // dispatch({
          //   type: "REGISTER_USER_SUCCESS",
          //   payload: resJson
          // });
        push("/customers");


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "REGISTER_USER_FAILURE"
          });
        });
      
    };

    return (
        


      <div className="container">
      <form>
     

        <label htmlFor="type">Type</label>
        <input
            id="type"
            name="type"
            type="text"
            value={state.type || ""}
            onChange={e => setState({...state, type: e.target.value})}
            className="text-input"
            />
        <br/>

        <label htmlFor="card-number">Card number</label>
        <input
            id="card-number"
            name="card-number"
            type="text"
            value={state.cardNumber || ""}
            onChange={e => setState({...state, cardNumber: e.target.value})}
            className="text-input"
            />
        <br/>

        <label htmlFor="expiration-month">Expiration month</label>
        <input
            id="expiration-month"
            name="expiration-month"
            type="text"
            value={state.expirationMonth || ""}
            onChange={e => setState({...state, expirationMonth: Number(e.target.value)})}
            className="text-input"
            />
        <br/>

        <label htmlFor="expiration-year">Expiration year</label>
        <input
            id="expiration-year"
            name="expiration-year"
            type="text"
            value={state.expirationYear || ""}
            onChange={e => setState({...state, expirationYear: Number(e.target.value)})}
            className="text-input"
            />
        <br/>

          <ButtonToolbar >

          <Button variant="primary" onClick={handleOnClick}>Save</Button>
          </ButtonToolbar>




      {/* <div className="form-error">
            <p>
              {state.customerHasError && "Error Creating customer!"}
            </p>
      </div>
      <div className="form-action clearfix">
          <button
            type="button"
            id="overlay-confirm-button"
            className="button button-primary"
            onClick={onSubmit}
            disabled={state.isButtonDisabled}
          >
            {state.isSubmitting ? "Submitting..." : "Submit"}
          </button>
          <button
            type="button"
            id="overlay-cancel-button"
            className="button button-default small close-overlay pull-right"
            onClick={onClose}
          >
                Cancel
          </button>
      </div>  */}
  </form>         
</div> 
        

        
  );
};

export default NewCreditCardComponent;


