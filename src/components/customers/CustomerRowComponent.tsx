import React from "react";
import { useHistory } from 'react-router'
import { AppContext } from "../../App";
import { City } from "../../model/City";
import { Customer } from "../../model/Customer";
import { State } from "../../model/State";
import { CustomerProps } from "../../props/CustomerProps";

interface CustomerRowComponentState {
  isClicked : boolean;
  customer : Customer;
  editingCustomer? : Customer;
  stateId?: number;
}
export const CustomerRowComponent : React.FunctionComponent<CustomerProps> = ({customer}) => {
  const { push } = useHistory();
  const { state : appState, dispatch } = React.useContext(AppContext) as any;
  const customerCity = appState.cities?.filter((city: City) => city.Id === customer?.CityId)[0];
  const stateId = customerCity ? customerCity.StateId : appState.states ? appState.states[0].Id : 1;

  const initialState : CustomerRowComponentState = {
    isClicked : false,
    customer : customer,
    editingCustomer : undefined,
    stateId 
  };
  const [state, setState] = React.useState(initialState);


  
  const handleOnClick = () => {
    if(appState.user?.Token) {

      push(`/customer/${customer.Id}/bills`);
      
      dispatch({
        type: "SHOW_CUSTOMER_BILLS_MODAL",
        payload: customer.Id
      });
    }
  };

  const handleOnDeleteClick = (event : any) => {
    event.stopPropagation();
    fetch(`http://www.fulek.com/nks/api/aw/deletecustomer`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${appState.user.Token}`
        },
        body: JSON.stringify({
          Id: customer.Id
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          dispatch({
            type: "SHOULD_FETCH_CUSTOMERS"
          });
        


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "REGISTER_USER_FAILURE"
          });
        });
    push("/customers");
  };
  

  const handleOnEditClick = (event : any) => {
    event.stopPropagation();
    const isClicked = !state.isClicked;
    
    const editingCustomer = {...state.customer};
    setState({...state, isClicked, editingCustomer : editingCustomer});
  };

  const handleOnSaveClick = (event : any) => {
    event.stopPropagation();
    const isClicked = false;


    fetch(`http://www.fulek.com/nks/api/aw/editcustomer`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${appState.user.Token}`
        },
        body: JSON.stringify({
          Id: state.editingCustomer?.Id,
          Name: state.editingCustomer?.Name,
          Surname: state.editingCustomer?.Surname,
          Email: state.editingCustomer?.Email,
          Telephone: state.editingCustomer?.Telephone,
          CityId: state.editingCustomer?.CityId,
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          dispatch({
            type: "EDIT_CUSTOMER_SAVE",
            payload: state.editingCustomer!
          });
          setState({...state, customer : state.editingCustomer!, editingCustomer : undefined, isClicked});


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "NEW_CUSTOMER_FAILURE"
          });
        });
      

    
  };

  const handleOnCancelClick = (event : any) => {
    event.stopPropagation();
    const isClicked = false;
    setState({...state, isClicked, editingCustomer : undefined});
  };
  
  const handleInputChange = (event : any) => {
    const updatedCustomer = state.editingCustomer!;
    (updatedCustomer as any)[event.target.name] = event.target.value;

    setState({
      ...state,
      editingCustomer: updatedCustomer
    });
  };
 

  return (
    state.isClicked ? 
    <tr style={{color: 'red'}}>
      <React.Fragment >
        <td>
          <input type="text" name="Id" value={state.editingCustomer?.Id.toString()} onChange={handleInputChange} />
        </td>
        <td>
          <input type="text" name="Name" value={state.editingCustomer?.Name.toString()} onChange={handleInputChange} />
        </td>
        <td>
          <input type="text" name="Surname" value={state.editingCustomer?.Surname.toString()} onChange={handleInputChange} />
        </td>
        <td>
          <input type="text" name="Email" value={state.editingCustomer?.Email.toString()} onChange={handleInputChange} />
        </td>
        <td>
          <input type="text" name="Telephone" value={state.editingCustomer?.Telephone.toString()} onChange={handleInputChange} />
        </td>
        <td>
          <select name="state-select" id="state" value={state.stateId} onChange={e => setState({...state, stateId: Number(e.target.value)})}>
                    {appState.states?.map((_state : State) => (
                      <option value={_state.Id} key={`state-${_state.Id}`}>{_state.Name}</option>
                    ))}
                    
          </select>
        </td>
        <td>
          <select name="city-select" id="city" onChange={e => setState({...state, editingCustomer: {...state.editingCustomer!, CityId: Number(e.target.value)}})}>
                    {appState.cities?.filter((city: City) => city.StateId === state.stateId).map((city : City) => (
                      <option value={city.Id} key={`city-${city.Id}`}>{city.Name}</option>
                    ))}
                    
          </select>
        </td>
        
        <td><button onClick={handleOnSaveClick}>save</button></td>
        <td><button onClick={handleOnCancelClick}>cancel</button></td>
      </React.Fragment>
    </tr>
    :
    <tr onClick={handleOnClick}>
    <React.Fragment >
      <td>{state.customer.Id}</td>
      <td>{state.customer.Name}</td>
      <td>{state.customer.Surname}</td>
      <td>{state.customer.Email}</td>
      <td>{state.customer.Telephone}</td>
      <td>{appState.cities?.filter((city: City) => city.Id === state.customer.CityId)[0]?.Name}</td>
      {
        appState.user?.Token && (
          <td>
          <div>
          <button onClick={handleOnEditClick}>edit</button>
          <button onClick={handleOnDeleteClick}>delete</button>
          </div>
          </td>
        )
      }
     
    </React.Fragment>
  </tr>
  ) 
};

export default CustomerRowComponent;
