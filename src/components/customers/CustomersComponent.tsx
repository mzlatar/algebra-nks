import React from "react";
import * as _ from "lodash";
import './CustomersComponent.css';
import { AppContext } from "../../App";
import { useHistory } from "react-router-dom";
import { FilterType } from "../../model/FilterType";
import { Button, ButtonToolbar, Table } from "react-bootstrap";
import CustomerBillsModalComponent from "../bills/CustomerBillsModalComponent";
import NewCustomerModalComponent from "./NewCustomerModal";
import CustomerRowComponent from "./CustomerRowComponent";


export const CustomersComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    const history = useHistory();
    const customerPageNumbers: number[] = [];
    const initialState = {
        customers : [],
        customerPageNumbers,
        search: ""
      };
    const [state, setState] = React.useState(initialState);
    
    const handlePageClick = (pageIndex : number) => {
        dispatch({type:"SET_CURRENT_PAGE", payload: pageIndex});
    };
    
    React.useEffect(() => {
        const customersFromPagination = appState.customersPagination?.customersPage[appState.customersPagination.currentPage]?.customers;
        console.log("pages from customes: " + appState.customersPagination?.totalPages);
        setState({...state, customers: customersFromPagination, customerPageNumbers : _.range(appState.customersPagination?.totalPages)});
      }, [appState.customersPagination]);

    const handleChangeFilterClick = (filterType : FilterType) => {
        dispatch({type:"SET_FILTER", payload: filterType});
    };

    const getTableHeaderClassName = (filterType : FilterType) => {
        return filterType === appState.filterType ? 
            appState.filterAscended ? 
                "active ascended"
                : "active"
            : ""

    };
    
    return (
        
        <div>
            <ButtonToolbar className="toolbar">

            <Button variant="primary" onClick={() => { 
                history.push("/customers/new");

                dispatch({
                    type: "SHOW_NEW_CUSTOMER_MODAL",
                });
            }}>New customer</Button>

            <Button variant="primary" onClick={() => { 
                history.push("/cards/new");

                
            }}>New card</Button>
            </ButtonToolbar>
            <input
            id="search"
            name="search"
            type="text"
            value={state.search || ""}
            onChange={e => {dispatch({type: "SEARCH_CUSTOMERS", payload: e.target.value}); setState({...state, search: e.target.value})}}
            className="text-input"
            />
         
            {
                        appState.customerBillsModalEnabled ? 
                        <CustomerBillsModalComponent show={appState.customerBillsModalEnabled} onHide={() => {
                            history.push("/customers");
            
                            dispatch({
                                type: "HIDE_CUSTOMER_BILLS_MODAL",
                            });
                        }}/>
                        : <></>
                            
            }

            {
                        appState.newCustomerModalEnabled ? 
                        <NewCustomerModalComponent show={appState.newCustomerModalEnabled} onHide={() => {
                            history.push("/customers");
            
                            dispatch({
                                type: "HIDE_NEW_CUSTOMER_MODAL",
                            });
                        }}/>
                        : <></>
                            
            }

            
            
            <div className="mt-5 d-flex justify-content-center">
             
                <Table responsive="lg" className="mt-4" striped bordered hover size="sm"> 
                <thead>
                    <tr>
                        <th onClick={() => handleChangeFilterClick(FilterType.Id)} className={getTableHeaderClassName(FilterType.Id) }>Id</th>
                        <th onClick={() => handleChangeFilterClick(FilterType.Name)} className={getTableHeaderClassName(FilterType.Name) }>Name</th>
                        <th onClick={() => handleChangeFilterClick(FilterType.Surname)} className={getTableHeaderClassName(FilterType.Surname) }>Surname</th>
                        <th onClick={() => handleChangeFilterClick(FilterType.Email)} className={getTableHeaderClassName(FilterType.Email) }>Email</th>
                        <th onClick={() => handleChangeFilterClick(FilterType.Telephone)} className={getTableHeaderClassName(FilterType.Telephone) }>Telephone</th>
                        <th onClick={() => handleChangeFilterClick(FilterType.City)} className={getTableHeaderClassName(FilterType.City) }>City</th>
        <th >{appState.user?.Token ? "Actions" : ""}</th>
                    </tr>
                </thead>
                <tbody>
                    
                    {
                        state.customers
                        && state.customers.map((customer : any) => (
                                <CustomerRowComponent customer={customer} key={customer.Id.toString()}/>
                                
                            ))
                    }
                    
                </tbody>
                </Table>
           
            
            </div>
            <div>

                
                <ul>
                    {
                        state.customerPageNumbers.map((pageNumber) => (
                            <li className={`page-item ${(pageNumber === appState.customersPagination.currentPage) ? "active" : ""}`} key={`page-${pageNumber}`}>
                                <a onClick={() => handlePageClick(pageNumber)}>{pageNumber + 1}</a>
                            </li>
                        ))
                    }
                </ul>
                <div>
                    <label htmlFor="items-per-page-select">Items per page</label>
                    <select name="items-per-page-select" id="items-per-page-select" value={appState.customersPagination.itemsPerPage} onChange={e => dispatch({type: "SET_ITEMS_PER_PAGE", payload:  Number(e.target.value)})}>
                                <option value={10}>10</option>
                                <option value={25}>25</option>
                                <option value={50}>50</option>
                                <option value={100}>100</option>
                                
                    </select>
                </div>
            </div>
        </div>
        

        
  );
};

export default CustomersComponent;
