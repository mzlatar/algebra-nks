import React from "react";
import { AppContext } from "../../App";
import { Customer } from "../../model/Customer";

interface NewCustomerState {
  customer?: Customer,
  customerHasError : boolean,
  isButtonDisabled : boolean
  isSubmitting : boolean
}

export const NewCustomerComponent : React.FunctionComponent = () => {
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
    // dispatch({type: "SHOW_MODAL"});
    return(<></>);
};
export default NewCustomerComponent;
