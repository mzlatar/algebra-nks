import React from "react";
import {Modal, Button, ButtonToolbar} from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { AppContext } from "../../App";
import { City } from "../../model/City";
import { Customer } from "../../model/Customer";
import { State } from "../../model/State";

interface NewCustomerState {
  customer?: Customer;
  customerHasError : boolean;
  isButtonDisabled : boolean;
  isSubmitting : boolean;
  stateId?: number;
}

interface NewCustomerModalProps {
    show : boolean;
    onHide : Function;
    animation? : boolean;
}

export const NewCustomerModalComponent : React.FunctionComponent<NewCustomerModalProps> = (props) => {

    const { state : appState, dispatch } = React.useContext(AppContext) as any;

    const initialState : NewCustomerState = {
      customer : undefined,
      customerHasError : false,
      isButtonDisabled : false,
      isSubmitting : false,
      stateId: appState.states[0].Id
    };

    const [state, setState] = React.useState(initialState);
    const { push } = useHistory();

    const onClose = () => {};
    
    const onSubmit = () => {
      console.log(state.customer!);
    };


    const handleOnClick = () => {
      
      dispatch({
        type: "NEW_CUSTOMER_REQUEST_STARTED",
      });


      fetch(`http://www.fulek.com/nks/api/aw/addcustomer`, {
        method: "post",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${appState.user.Token}`
        },
        body: JSON.stringify({
          Name: state.customer?.Name,
          Surname: state.customer?.Surname,
          Email: state.customer?.Email,
          Telephone: state.customer?.Telephone,
          CityId: state.customer?.CityId,
        })
      })
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((resJson) => {
          console.log(resJson);
        
          dispatch({
            type: "NEW_CUSTOMER_SUCCESS",
            payload: resJson
          });
        push("/customers");


        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: "NEW_CUSTOMER_FAILURE"
          });
        });
      
    };

  return (
    <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            New customer
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="container">
                <form>

                  <label htmlFor="name">Name</label>
                      <input
                      id="name"
                      name="name"
                      type="text"
                      value={state.customer?.Name || ""}
                      onChange={e => setState({...state, customer: {...state.customer!, Name : e.target.value}})}
                      className="text-input"
                      />
                  <br/>
                  <label htmlFor="surname">Surname</label>
                      <input
                      id="surname"
                      name="surname"
                      type="text"
                      value={state.customer?.Surname || ""}
                      onChange={e => setState({...state, customer: {...state.customer!, Surname : e.target.value}})}
                      className="text-input"
                      />

                  <br/>
                  <label htmlFor="email">Email</label>
                      <input
                      id="email"
                      name="email"
                      type="text"
                      value={state.customer?.Email || ""}
                      onChange={e => setState({...state, customer: {...state.customer!, Email : e.target.value}})}
                      className="text-input"
                      />
                  <br/>
                  <label htmlFor="telephone">Telephone</label>
                      <input
                      id="telephone"
                      name="telephone"
                      type="text"
                      value={state.customer?.Telephone || ""}
                      onChange={e => setState({...state, customer: {...state.customer!, Telephone : e.target.value}})}
                      className="text-input"
                      />
                    
                  
                  <br/>
                 

                  <label htmlFor="state-select">State</label>
                  <select name="state-select" id="state" onChange={e => setState({...state, stateId: Number(e.target.value)})}>
                            {appState.states?.map((state : State) => (
                              <option value={state.Id} key={`state-${state.Id}`}>{state.Name}</option>
                            ))}
                            
                  </select>

                  <br/>
                  <label htmlFor="city-select">City</label>
                  <select name="city-select" id="city" onChange={e => setState({...state, customer: {...state.customer!, CityId : Number(e.target.value)}})}>
                    {appState.cities?.filter((city: City) => city.StateId === state.stateId).map((city : City) => (
                      <option value={city.Id} key={`city-${city.Id}`}>{city.Name}</option>
                    ))}
                    
                  </select>
                  <br/>

                {/* <div className="form-error">
                      <p>
                        {state.customerHasError && "Error Creating customer!"}
                      </p>
                </div>
                <div className="form-action clearfix">
                    <button
                      type="button"
                      id="overlay-confirm-button"
                      className="button button-primary"
                      onClick={onSubmit}
                      disabled={state.isButtonDisabled}
                    >
                      {state.isSubmitting ? "Submitting..." : "Submit"}
                    </button>
                    <button
                      type="button"
                      id="overlay-cancel-button"
                      className="button button-default small close-overlay pull-right"
                      onClick={onClose}
                    >
                          Cancel
                    </button>
                </div>  */}
            </form>    
            <ButtonToolbar >

              <Button variant="primary" onClick={handleOnClick}>Save</Button>
              </ButtonToolbar>     
        </div> 
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => dispatch({
                    type: "HIDE_MODAL",
                })}>Close</Button>
        </Modal.Footer>
      </Modal>
  );
};
export default NewCustomerModalComponent;


  
//   function App() {
//     const [modalShow, setModalShow] = React.useState(false);
  
//     return (
//       <>
//         <Button variant="primary" onClick={() => setModalShow(true)}>
//           Launch vertically centered modal
//         </Button>
  
//         <MyVerticallyCenteredModal
//           show={modalShow}
//           onHide={() => setModalShow(false)}
//         />
//       </>
//     );
//   }
  
//   render(<App />);
