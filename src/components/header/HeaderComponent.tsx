import React from "react";
import {NavLink} from "react-router-dom";
import {Navbar, Nav} from "react-bootstrap";
import NavbarToggle from "react-bootstrap/NavbarToggle";
import { AppContext } from "../../App";
import './Header.css';

export const HeaderComponent : React.FunctionComponent = () => {
   
  const { state : appState, dispatch } = React.useContext(AppContext) as any;

     

  return (
    
    <div className="container">
        <Navbar bg="dark" expand="lg" variant="dark">
        <NavbarToggle aria-controls="basic-navbar-nv"></NavbarToggle>
        <Navbar.Brand href="/" >AdventureWorks Management app</Navbar.Brand>
        {/* <h3 className="m-3 d-flex justify-content-center text-white"> AdventureWorks Management app</h3> */}
        <Navbar.Collapse >
        <Nav>
            <NavLink className="d-inline p-2 bg-dark text-white" to="/">Customers</NavLink>
            <NavLink className="d-inline p-2 bg-dark text-white" to="/customers/new">New</NavLink>
           
           

            
        </Nav>
    
        </Navbar.Collapse>
        <Nav className="justify-content-end">
        {
              appState.user?.Username && appState.user?.Token ? 
              <Navbar.Text>
                  Signed in as: <a href="/user/edit">{appState.user?.Username}</a>
                  <NavLink className="d-inline p-2 bg-dark text-white" to="/logout">Logout</NavLink>
              </Navbar.Text>
                :
                <div>
                  <NavLink className="d-inline p-2 bg-dark text-white" to="/register">Register</NavLink>
                  <NavLink className="d-inline p-2 bg-dark text-white" to="/login">Login</NavLink>
                </div> 

            }
            </Nav>
        </Navbar>

       
    </div>
  );
};

export default HeaderComponent;
