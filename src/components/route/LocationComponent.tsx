import React from "react";
import {useLocation} from "react-router-dom";
import { AppContext } from "../../App";

export const LocationComponent : React.FunctionComponent = () => {
  const { state : appState, dispatch } = React.useContext(AppContext) as any;
  const location = useLocation();
  React.useEffect(() => {
    console.log(JSON.stringify(location))
  
    if(appState.user?.Token) {
      if(location.pathname === "/customers/new") {
        dispatch({type:"SHOW_NEW_CUSTOMER_MODAL"})
      }  else if(location.pathname.indexOf("bills/new") > -1) {
        //do nothing
      }
      else if(location.pathname.indexOf("bills") > -1) {
        const customerId = location.pathname.split("/")[2];
        console.log("customerId from location: " + customerId);
        dispatch({
          type: "SHOW_CUSTOMER_BILLS_MODAL",
          payload: customerId
        });
      } 
      else if(location.pathname.indexOf("bill") > -1) {
        const billId = location.pathname.split("/")[2];
        console.log("billId from location: " + billId);
        dispatch({
          type: "SHOW_CUSTOMER_BILL_DETAILS",
          payload: billId
        });
      } else if(location.pathname === "/customers") {
        dispatch({type:"HIDE_NEW_CUSTOMER_MODAL"});
        dispatch({type:"HIDE_CUSTOMER_BILLS_MODAL"});
      }
    } else {
      return;
    }
   
    
  }, [location]);
     
       
  

  return (
    <>
    </>
  );
};

export default LocationComponent;
