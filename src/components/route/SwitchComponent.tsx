import React from "react";
import { Redirect, Route, Switch, useHistory, useLocation } from "react-router-dom";
import { AppContext } from "../../App";
import EditUserComponent from "../authentication/EditUserComponent";
import LoginComponent from "../authentication/LoginComponent";
import LogoutComponent from "../authentication/LogoutComponent";
import RegisterComponent from "../authentication/RegisterComponent copy";
import BillDetailsComponent from "../bills/BillDetailsComponent";
import CustomerBillsModalComponent from "../bills/CustomerBillsModalComponent";
import NewBillComponent from "../bills/NewBillComponent";
import NewBillItemComponent from "../bills/NewBillItemComponent";
import NewCreditCardComponent from "../credit-card/NewCreditCardComponent";
import CustomerDetailComponent from "../customers/CustomerDetailComponent";
import CustomersComponent from "../customers/CustomersComponent";
import NewCustomerModalComponent from "../customers/NewCustomerModal";



export const SwitchComponent : React.FunctionComponent = (props : any) => {
    let location = useLocation();
    const { state : appState, dispatch } = React.useContext(AppContext) as any;
   
    const history = useHistory();
    

  return (
    
    <>
        <Switch location={location}>
          <Route path="/bill/:id" render={() => 
              <BillDetailsComponent/> 
          }/>

          <Route path="/bills/new" render={() => appState.user?.Token ?
              <NewBillComponent/> : <Redirect to="/customers" />
          }/>

          <Route path="/cards/new" render={() => appState.user?.Token ?
              <NewCreditCardComponent/> : <Redirect to="/customers" />
          }/>


          <Route path="/bill/:id/new" render={() => appState.user?.Token ?
              <NewBillItemComponent/> : <Redirect to="/customers" />
          }/>

          <Route path="/user/edit" render={() => 
              <EditUserComponent/> 
          }/>

          <Route path="/logout" render={() => appState.user?.Token ?
              <LogoutComponent/> : <Redirect to="/customers" />
          }/>

          
          <Route path="/register">
            <RegisterComponent />
          </Route>
        
          <Route path="/login">
            <LoginComponent />
          </Route>

          <Route
            path="/customers/new"
            children={({ match }) => {
              if (appState.user?.Token) {
                return (
                  <CustomersComponent >
                 {/* id={match && match.params.id} */}
                <NewCustomerModalComponent show={Boolean(match)} onHide={() => {
                  history.push("/customers");
  
                  dispatch({
                      type: "HIDE_NEW_CUSTOMER_MODAL",
                  });
              }}/>
              </CustomersComponent>
                )
              } else {
              return (
                <Redirect to="/customers" />
              )
              }
            }}
          />


          <Route
            path="/customer/:id/bills"
            children={({ match }) => {
              console.log(JSON.stringify(match));
              return (
                <CustomersComponent >
                <CustomerBillsModalComponent customerId={500}  show={Boolean(match)} onHide={() => {
                  history.push("/customers");
  
                  dispatch({
                      type: "HIDE_CUSTOMER_BILLS_MODAL",
                  });
              }}/>
              </CustomersComponent>
              );
            }}
          />


          <Route path={["/customers", "/"]}>
            <CustomersComponent />
          </Route>
        </Switch>


      
    </>
  );
};

export default SwitchComponent;
