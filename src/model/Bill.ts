import { CreditCard } from "./CreditCard";
import { Seller } from "./Seller";

export type Bill = {
    Id : string;
    Date : Date;
    BillNumber : string;
    CustomerId : number;
    SellerId : number;
    CreditCardId : number;
    Comment? : string;
    CreditCard : CreditCard;
    Seller : Seller;

}

