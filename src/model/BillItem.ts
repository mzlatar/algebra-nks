import { CreditCard } from "./CreditCard";
import { Product } from "./Product";
import { Seller } from "./Seller";

export type BillItem = {
    Id : number;
    BillId : number;
    Quantity : number;
    ProductId : number;
    PricePerPiece : number;
    TotalPrice : number;
    Product : Product;
}


// {"Id":34979,"BillId":50744,"Quantity":2,"ProductId":817,"PricePerPiece":180.1290,"TotalPrice":360.258000,"Product":{"Id":817,"Name":"HL Mountain Front Wheel",
// "ProductNumber":"FW-M928","Color":"Black","ProductSubcategoryID":17}},