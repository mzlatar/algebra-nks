import { CreditCard } from "./CreditCard";
import { Seller } from "./Seller";

export type Category = {
    Id : string;
    Name : string;
}