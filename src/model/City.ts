import { CreditCard } from "./CreditCard";
import { Seller } from "./Seller";

export type City = {
    Id : number;
    Name : string;
    StateId : number;
}

