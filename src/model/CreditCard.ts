export type CreditCard = {
    Id : string;
    Type : CreditCardType;
    CardNumber : number;
    ExpirationMonth : number;
    ExpirationYear : number;

}

export enum CreditCardType {
    Visa,
    American
}


