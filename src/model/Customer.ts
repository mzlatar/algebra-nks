export type Customer = {
    Id : string;
    Name : string;
    Surname : string;
    Email : string;
    Telephone : string;
    CityId : number;
    City: string;

}