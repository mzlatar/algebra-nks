import { Customer } from "./Customer";

export type CustomerPage = {
    
    customers: Customer[];
}