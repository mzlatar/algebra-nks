import { CustomerPage } from "./CustomerPage";

export type CustomersPagination = {
    itemsPerPage : number;
    currentPage: number;
    totalPages: number;
    customersPage: CustomerPage[];
}