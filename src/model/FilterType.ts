export enum FilterType {
    Id,
    Name,
    Surname,
    Email,
    Telephone,
    City,
}