import { CreditCard } from "./CreditCard";
import { Seller } from "./Seller";

export type Product = {
    Id : number;
    Name : string;
    ProductNumber : Date;
    Color : string;
    ProductSubcategoryId : number;
}
