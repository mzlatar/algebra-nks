export type Seller = {
    Id : string;
    Name : string;
    Surname : string;
    PermanentEmployee : boolean;

}
