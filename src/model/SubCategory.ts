import { CreditCard } from "./CreditCard";
import { Seller } from "./Seller";

export type SubCategory = {
    Id : string;
    Name : string;
    CategoryId: number;
}