export type User = {
    Id : Number;
    Name : string;
    Username : string;
    Password : string;
    Token : string;
    Img : string;
}
