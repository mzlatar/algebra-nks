import { Bill } from "../model/Bill";
import { BillItem } from "../model/BillItem";

export interface BillItemProps {
    billItem: BillItem
  };
  